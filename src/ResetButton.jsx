import { ResetIcon } from "@radix-ui/react-icons";

export default function ResetButton({ setCount }) {
  const handleReset = (e) => {
    setCount(0);
    e.currentTarget.blur();
  };

  return (
    <button className="reset-btn">
      <ResetIcon onClick={handleReset} className="reset-btn-icon" />
    </button>
  );
}
