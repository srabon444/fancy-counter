# Fancy Counter

This counter is created using React.js with Vite, Javascript, and CSS


## [Live Application](https://fancy-counter-xi.vercel.app/)

![App Screenshot 1](public/1.png)
![App Screenshot 2](public/2.png)
![App Screenshot 3](public/3.png)

## Features

- Count Increase by clicking "+" sign or, pressing spacebar on the keyboard
- Count Decrease by clicking "-" sign.
- Count goes to "0" Zero by clicking reset icon.

## Tech Stack
**Fronted:**
- React Js
- Javascript
- CSS

**Hosting:**
- Vercel


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/fancy-counter.git
```

Install dependencies

```bash
  npm install
```


